﻿namespace EscuelaNet.Dominio.Clientes
{
    public enum EstadoSolicitud
    {
        Borrador,
        Desarrollo,
        Produccion,
        Deprecado,
        Abandonado
    }
}
