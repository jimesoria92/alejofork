﻿using EscuelaNet.Dominio.SeedWoork;
using System.Collections.Generic;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Tema : Entity, IAggregateRoot
    {
        public string Nombre { get; set; }
        public NivelTema Nivel { get; set; }
        public IList<Instructor> Instructores { get; set; }
        private Tema()
        {

        }

        public Tema(string nombre, NivelTema nivel) : this()
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Nivel = nivel;
        }
    }
}