﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Persona /*: Entity*/
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Dni { get; set; }

        [DisplayName("Fecha de Nacimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FechaNacimiento { get; set; }
    }
}
