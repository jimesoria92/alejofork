﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
    public interface IProgramadorRepository : IRepository<Programador>
    {
        Programador Add(Programador programador);
        void Update(Programador programador);
        void Delete(Programador programador);
        Programador GetProgramador(int id);
        List<Programador> ListProgramador();
    }
}
