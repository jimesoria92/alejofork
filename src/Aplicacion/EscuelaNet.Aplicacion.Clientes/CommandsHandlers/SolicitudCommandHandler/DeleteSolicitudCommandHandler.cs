﻿using EscuelaNet.Aplicacion.Clientes.Commands.SolicitudCommand;
using EscuelaNet.Aplicacion.Clientes.Responds;
using EscuelaNet.Dominio.Clientes;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.CommandsHandlers.SolicitudCommandHandler
{
    public class DeleteSolicitudCommandHandler :
        IRequestHandler<DeleteSolicitudCommand, CommandRespond>
    {
        private ISolicitudRepository _solicitudesRepositorio;

        public DeleteSolicitudCommandHandler(ISolicitudRepository solicitudesRepositorio)
        {
            _solicitudesRepositorio = solicitudesRepositorio;
        }
        
        public Task<CommandRespond> Handle(DeleteSolicitudCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            try
            {
                var solicitud = _solicitudesRepositorio.GetSolicitud(request.IdSolicitud);
                _solicitudesRepositorio.Delete(solicitud);
                _solicitudesRepositorio.UnitOfWork.SaveChanges();
                responde.Succes = true;
                return Task.FromResult(responde);

            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }
        }
    }
}
