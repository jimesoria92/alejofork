﻿using EscuelaNet.Aplicacion.Programadores.Responds;
using EscuelaNet.Dominio.Programadores;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.Commands
{
    public class DeleteProgramadorCommand : IRequest<CommandRespond>
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Legajo { get; set; }
        public string Dni { get; set; }
        public string Rol { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public EstadoDeDisponibilidad Disponibilidad { get; set; }

    }
}
