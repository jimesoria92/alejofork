﻿using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Programadores.Web.Models
{
    public class NuevoSkillsEquipoModel
    {
        public int IdSkillsEquipo { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public Experiencia Grados { get; set; }
        public int IdProgramadores { get; set; } 
        public int idEquipos { get; set; }
        
    }
}