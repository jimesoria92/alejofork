﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Presentacion.Capacitacion.Web.Models;
using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Aplicacion.Capacitaciones.QueryServices;
using EscuelaNet.Aplicacion.Capacitaciones.QueryServices.InstructoresServices;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.InstructorCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.InstructorTemaCommand;
using MediatR;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Controllers
{
    public class InstructoresController : Controller
    {
        private IInstructorRepository _instructorRepository;
        private IInstructoresQuery _instructoresQuery;
        private IMediator _mediator;
        private ITemaRepository _temaRepository;
        private ITemasQuery _temasQuery;

        public InstructoresController(IInstructorRepository instructorRepository,
            IInstructoresQuery instructoresQuery, IMediator mediator, ITemaRepository temaRepository, ITemasQuery temasQuery)
        {
            _instructorRepository = instructorRepository;
            _instructoresQuery = instructoresQuery;
            _mediator = mediator;
            _temaRepository = temaRepository;
            _temasQuery = temasQuery;
        }
        // GET: Instructor
        public ActionResult Index()
        {
            var instructores = _instructoresQuery.ListInstructores();

            var model = new InstructorIndexModel()
            {
                Titulo = "Instructores",
                Instructores = instructores,

            };
            return View(model);
        }


        // GET: Instructor/New
        public ActionResult New()
        {
            var model = new NuevoInstructorModel()
            {
                FechaNacimiento = DateTime.Now
            };
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public async Task<ActionResult> New(NuevoInstructorCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Instructor creado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoInstructorModel()
                {
                    Nombre = model.Nombre,
                    Apellido = model.Apellido,
                    Dni = model.Dni,
                    FechaNacimiento = model.FechaNacimiento,
                };
                return View(modelReturn);
            }

        }

        // GET: Instructor/Edit/5
        public ActionResult Edit(int id)
        {
            var instructor = _instructoresQuery.GetInstructor(id);
            var model = new NuevoInstructorModel()
            {
                Nombre = instructor.Nombre,
                Id = id,
                Apellido = instructor.Apellido,
                Dni = instructor.Dni,
                FechaNacimiento = instructor.FechaNacimiento,
            };
            return View(model);
        }

        // POST: Instructor/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(UpdateInstructorCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Instructor Editado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoInstructorModel()
                {
                    Nombre = model.Nombre,
                    Id = model.Id,
                    Apellido = model.Apellido,
                    Dni = model.Dni,
                    FechaNacimiento = model.FechaNacimiento,
                };
                return View(modelReturn);
            }
        }

        // GET: Instructor/Delete/5
        public ActionResult Delete(int id)
        {
            var instructor = _instructoresQuery.GetInstructor(id);

            var model = new NuevoInstructorModel()
            {
                Nombre = instructor.Nombre,
                Id = id,
                Apellido = instructor.Apellido,
                Dni = instructor.Dni,
                FechaNacimiento = instructor.FechaNacimiento,
            };
            return View(model);
        }

        // POST: Instructor/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(DeleteInstructorCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Instructor Elimninado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var model2 = new NuevoInstructorModel()
                {
                    Nombre = model.Nombre,
                    Id = model.Id,
                    Apellido = model.Apellido,
                    Dni = model.Dni,
                    FechaNacimiento = model.FechaNacimiento,
                };
                return View(model2);
            }
        }

        public ActionResult Temas(int id)
        {
            var instructor = _instructoresQuery.GetInstructor(id);
            var temas = _temasQuery.ListTemasInstructor(id);

            var model = new InstructorTemaModel()
            {
                Instructor = instructor,
                Temas = temas,
            };
            return View(model);
        }
  
       
       
        public ActionResult NewTema(int id)
        {
            var instructor = _instructoresQuery.GetInstructor(id);
            var temas = _temasQuery.ListTemas();
            var model = new NuevoInstructorTemaModel()
            {
                IDInstructor = id,
                NombreApellido = instructor.Nombre + " " + instructor.Apellido,
                Temas = temas
            };
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public async Task<ActionResult> NewTema(NuevoInstructorTemaCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Agregado el Tema al Instructor";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoInstructorTemaModel()
                {
                    IDTema = model.IDTema,
                    IDInstructor = model.IDInstructor, 
                    NombreTema = model.NombreTema,
                    NombreApellido = model.NombreApellido,
                };
                return View(modelReturn);
            }


        }


        public ActionResult DeleteTema(int id, int instructor)
        {
            var instructorBuscado = _instructoresQuery.GetInstructor(instructor);
            var temabuscado = _temasQuery.GetTema(id);
            var model = new NuevoInstructorTemaModel()
            {
                NombreTema = temabuscado.Nombre + " " + temabuscado.Nivel,
                IDInstructor = instructor,
                NombreApellido = instructorBuscado.Nombre + " " + instructorBuscado.Apellido,
                IDTema = id,
            };
            return View(model);
        }

        // POST: Instructor/Delete/5
        [HttpPost]
        public async Task<ActionResult> DeleteTema(DeleteInstructorTemaCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Tema del Instructor Eliminado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var model2 = new NuevoInstructorTemaModel();
                {
                   
                };
                return View(model2);
            }
        }

    }
}