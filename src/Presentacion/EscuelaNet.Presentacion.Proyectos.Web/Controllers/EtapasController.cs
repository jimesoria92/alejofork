﻿using EscuelaNet.Presentacion.Proyectos.Web.Infraestructura;
using EscuelaNet.Presentacion.Proyectos.Web.Models;
using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Proyectos.Web.Controllers
{
    public class EtapasController : Controller
    {
        private ILineaRepository _lineaRepository;
        public EtapasController(ILineaRepository lineaRepository)
        {
            _lineaRepository = lineaRepository;
        }
        // GET: Etapa
        public ActionResult Index(int idLinea, int idProy)
        {

            var proyecto = _lineaRepository.GetProyecto(idProy);
            if(proyecto.Etapas == null)
            {
                TempData["error"] = "Proyecto sin etapas";
                return RedirectToAction("../Proyectos/Index/?id=" + idLinea);
            }
            var model = new EtapaIndexModel() {
                Etapas = proyecto.Etapas.ToList(),
                IDLinea = idLinea,
                IDProy = idProy
            };

            return View(model);

        }

        public ActionResult New(int id)
        {
            var proyecto = _lineaRepository.GetProyecto(id);
            var model = new NuevaEtapaModel() { IDProy = id };
            return View(model);
        }

        [HttpPost]

        public ActionResult New(NuevaEtapaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var proyecto = _lineaRepository.GetProyecto(model.IDProy);

                    proyecto.PushEtapa(model.Nombre, model.duracion);
                    _lineaRepository.Update(proyecto.LineaDeProduccion);
                    _lineaRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "La etapa se ha creado";
                    return RedirectToAction("Index", new { idLinea = model.IDLinea, idProy = model.IDProy });
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            } else
            {
                TempData["error"] = "Debe completar ambos campos ";
                return View(model);
            }
        }

        public ActionResult Edit(int idProy, int idLinea, int idEtapa)
        {
            var etapas = _lineaRepository.GetEtapa(idEtapa);
            var proyecto = _lineaRepository.GetProyecto(etapas.IDProyecto);

            var model = new NuevaEtapaModel()
            {
                Nombre = etapas.Nombre,
                ID = idEtapa,
                duracion = etapas.Duracion,
                IDLinea = idLinea,
                IDProy = idProy
            };
            return View(model);
        }

        [HttpPost]

        public ActionResult Edit(NuevaEtapaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var proyecto = _lineaRepository.GetProyecto(model.IDProy);
                    proyecto.Etapas.Where(e => e.ID == model.ID).First().Nombre = model.Nombre;
                    proyecto.Etapas.Where(e => e.ID == model.ID).First().Duracion = model.duracion;

                    _lineaRepository.Update(proyecto.LineaDeProduccion);
                    _lineaRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "Etapa modificada";
                    return RedirectToAction("index", new { idLinea=model.IDLinea,idProy=model.IDProy }); 
                }
                catch(Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Debe completar ambos campos";
                return View(model);
            }
        }

        public ActionResult Delete(int idLinea, int idProy, int idEtapa)
        {
            var etapa = _lineaRepository.GetEtapa(idEtapa);
            var proyecto = _lineaRepository.GetProyecto(etapa.IDProyecto);
            var model = new NuevaEtapaModel()
            {
                Nombre = etapa.Nombre,
                ID = idEtapa
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(NuevaEtapaModel model)
        {
            try
            {
                var etapa = _lineaRepository.GetEtapa(model.ID);
                _lineaRepository.DeleteEtapa(etapa);
                _lineaRepository.UnitOfWork.SaveChanges();
                TempData["success"] = "Etapa eliminada";
                return RedirectToAction("Index", new { idLinea=model.IDLinea, idProy = model.IDProy });
            }
            catch(Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }



    }
}