﻿using EscuelaNet.Presentacion.Proyectos.Web.Infraestructura;
using EscuelaNet.Presentacion.Proyectos.Web.Models;
using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Proyectos.Web.Controllers
{
    public class TecnologiasController : Controller
    {
        // GET: Tecnologias
        public ActionResult Index()
        {
            var Tecno = Contexto.Instancia.Tecnologias;
            var model = new TecnologiasIndexModel()
            {
                Nombre = "Prueba",
                Tecnologias = Tecno
            };
            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevaTecnologiaModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevaTecnologiaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.Tecnologias.Add(new Tecnologias(model.Nombre));
                    TempData["success"] = "Tecnología creada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Campo nombre vacío";
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            var Tecno = Contexto.Instancia.Tecnologias[id];
            var model = new NuevaTecnologiaModel()
            {
                Nombre = Tecno.nombre,
                ID = id

            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(NuevaTecnologiaModel model)
        {
            if(!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.Tecnologias[model.ID].nombre = model.Nombre;
                    TempData["success"] = "Tecnología editada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Campo nombre vacío";
                    return View(model);
            }
        }

        public ActionResult Delete(int id)
        {

            var Tecno = Contexto.Instancia.Tecnologias[id];
            var model = new NuevaTecnologiaModel()
            {
                Nombre = Tecno.nombre,
                ID = id
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Delete(NuevaTecnologiaModel model)
        {
            try
            {
                Contexto.Instancia.Tecnologias.RemoveAt(model.ID);
                TempData["success"] = "Tecnología eliminada";
                return RedirectToAction("Index");
            }catch(Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}