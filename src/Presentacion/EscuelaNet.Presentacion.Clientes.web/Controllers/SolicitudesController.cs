﻿using EscuelaNet.Aplicacion.Clientes.Commands.SolicitudCommand;
using EscuelaNet.Aplicacion.Clientes.QueryServices;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using MediatR;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class SolicitudesController : Controller
    {
        private ISolicitudesQuery _solicitudesQuery;
        private IUnidadesQuery _unidadesQuery;
        private IMediator _mediator;

        public SolicitudesController(IMediator mediator,                
                IUnidadesQuery unidadesQuery,
                ISolicitudesQuery solicitudesQuery)
        {           
            _solicitudesQuery = solicitudesQuery;
            _unidadesQuery = unidadesQuery;
            _mediator = mediator;
        }

        public ActionResult Index()
        {
                var solicitudes = _solicitudesQuery.ListSolicitud();                      
                var model = new SolicitudesIndexModel()
                {
                    Titulo = "Solicitudes",
                    Solicitudes = solicitudes,                    
                };

                return View(model);                          
        }

        public ActionResult New()
        {                
            var model = new NuevaSolicitudModel()
            {
                Title = "Nueva Solicitud ",
            };
            return View(model);                        
        }

        [HttpPost]
        public async Task<ActionResult> New(NuevaSolicitudCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Solicitud creada";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaSolicitudModel()
                {
                    Titulo=model.Titulo,
                    Descripcion=model.Descripcion,
                    Estado=model.Estado,                    
                };
                return View(modelReturn);
            }            
        }

        public ActionResult Edit(int id)
        {
            var solicitud = _solicitudesQuery.GetSolicitud(id);
            if (solicitud==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {    
                var model = new NuevaSolicitudModel()
                {
                    Title = "Editar la solicitud '" + solicitud.Titulo+"'",
                    IdSolicitud = id,
                    Titulo = solicitud.Titulo,
                    Descripcion = solicitud.Descripcion,
                    Estado = solicitud.Estado
                };

                return View(model);
            }
            
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditSolicitudCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Solicitud Editada";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaSolicitudModel()
                {
                    Titulo = model.Titulo,
                    Descripcion = model.Descripcion,
                    Estado = model.Estado
                };
                return View(modelReturn);
            }            
        }

        public ActionResult Delete(int id)
        {
            var solicitud = _solicitudesQuery.GetSolicitud(id);

            if (solicitud==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {             
                var model = new NuevaSolicitudModel()
                {
                    IdSolicitud = id,
                    Titulo = solicitud.Titulo,
                    Descripcion = solicitud.Descripcion,
                    Estado = solicitud.Estado
                };

                return View(model);
            }               
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteSolicitudCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Solicitud Borrada";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaSolicitudModel()
                {
                    Titulo = model.Titulo,
                    Descripcion = model.Descripcion,
                    Estado = model.Estado
                };
                return View(modelReturn);
            }            
        }

        public ActionResult Unidades(int id)
        {

            var solicitud = _solicitudesQuery.GetSolicitud(id);
            var unidades = _solicitudesQuery.ListUnidadesDeSolicitud(id);
            var model = new UnidadSolicitudModel()
            {
                Solicitud = solicitud,
                Unidades = unidades
            };

            return View(model);

        }

        public ActionResult LinkUnidad(int id)
        {
            var solicitud = _solicitudesQuery.GetSolicitud(id);
            var unidades = _unidadesQuery.ListTodasUnidades();

            var model = new NuevaUnidadSolicitudModel()
            {
                IDSolicitud = id,
                TituloSolicitud = solicitud.Titulo,
                Unidades = unidades
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> LinkUnidad(LinkUnidadCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Solicitud Vinculada";
                return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
            }
            else
            {
                TempData["error"] = exito.Error;
                return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
            }

        }

        public ActionResult UnlinkUnidad(int id, int solicitud)
        {            
            var solicitudBuscada = _solicitudesQuery.GetSolicitud(solicitud);
            var unidadBuscada = _solicitudesQuery.FindUnidadEnSolicitud(id, solicitud);

            if (unidadBuscada != null && solicitudBuscada != null)
            {
                var model = new NuevaUnidadSolicitudModel()
                {
                    IDSolicitud = solicitud,
                    IDUnidad = id,
                    TituloSolicitud = solicitudBuscada.Titulo,
                    RazonSocialUnidad = unidadBuscada.RazonSocial
                };
                return View(model);

            }
            else
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Solicitudes/index");
            }
        }

        [HttpPost]
        public async Task<ActionResult> UnlinkUnidad(UnlinkUnidadCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {
                TempData["success"] = "Solicitud Desvinculada";
                return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
            }
            else
            {
                TempData["error"] = exito.Error;
                return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
            }            
        }     
        
    }
}