﻿using EscuelaNet.Aplicacion.Clientes.QueryModels;
using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Models
{
    public class UnidadSolicitudModel
    {
        public SolicitudesQueryModel Solicitud { get; set; }

        public List<UnidadesQueryModel> Unidades { get; set; }

        public UnidadesQueryModel Unidad { get; set; }

        public List<SolicitudesQueryModel> Solicitudes { get; set; }

    }
}