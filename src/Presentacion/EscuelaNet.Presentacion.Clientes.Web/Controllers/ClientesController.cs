﻿using EscuelaNet.Aplicacion.Clientes.Commands.ClienteCommand;
using EscuelaNet.Aplicacion.Clientes.QueryServices;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using MediatR;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class ClientesController : Controller
    {       
        private IClientesQuery _clientesQuery;
        private IMediator _mediator;

        public ClientesController(IMediator mediator, IClientesQuery clientesQuery)
        {
            _mediator = mediator;           
            _clientesQuery = clientesQuery;
        }
                
        public ActionResult Index()
        {
            var clientes = _clientesQuery.ListCliente();

            var model = new ClientesIndexModel()
            {
                Titulo = "Index clientes",
                Clientes = clientes
            };

            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevoClienteModel();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> New(NuevoClienteCommand model)
        {
            var exito = await _mediator.Send(model);

            if (exito.Succes)
            {                
                TempData["success"] = "Cliente creado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoClienteModel()
                {
                    RazonSocial = model.RazonSocial,
                    Email=model.Email,
                    Categoria=model.Categoria
                };
                return View(modelReturn);
            }             
        }

        public ActionResult Edit(int id)
        {
            var cliente = _clientesQuery.GetCliente(id);
            if (cliente==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {       
                var model = new NuevoClienteModel()
                {
                    Id = id,
                    RazonSocial = cliente.RazonSocial,
                    Email = cliente.Email,
                    Categoria = cliente.Categoria
                };
            return View(model);
            }
        }

        [HttpPost]       
        public async Task<ActionResult> Edit(EditClienteCommand model)
        {

            var exito = await _mediator.Send(model);
            if(exito.Succes)
            {                
                TempData["success"] = "Cliente editado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoClienteModel()
                {
                    Id = model.Id,
                    RazonSocial = model.RazonSocial,
                    Email = model.Email,
                    Categoria = model.Categoria
                };

                return View(modelReturn);
            }
        }

        public ActionResult Delete(int id)
        {
            var cliente = _clientesQuery.GetCliente(id);
            if (cliente==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {                      
                var model = new NuevoClienteModel()
                {
                    Id = id,
                    RazonSocial = cliente.RazonSocial,
                    Email = cliente.Email,
                    Categoria = cliente.Categoria
                };
                return View(model);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteClienteCommand model)
        {            
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Cliente borrado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoClienteModel()
                {
                    Id = model.Id,
                    RazonSocial = model.RazonSocial,
                    Email = model.Email,
                    Categoria = model.Categoria
                };
                return View(modelReturn);
            }              
        }       

    }
}