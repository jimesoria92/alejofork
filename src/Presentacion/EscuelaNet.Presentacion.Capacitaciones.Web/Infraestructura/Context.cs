﻿using EscuelaNet.Dominio.Capacitaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Capacitaciones.Web.Infraestructura
{
    public sealed class Context
    {
        private static Context _intancia = new Context();

        public static Context Instancia => _intancia;

        public List<Instructor> Instructores { get; set; }

        private Context()
        {
            this.Instructores = new List<Instructor>();
            Instructores.Add(new Instructor()
            {
                Nombre = "Matias",
                Apellido = "Juarez",
                Dni = "41222333",
                FechaNacimiento = DateTime.Now,
                Temas = new List<Tema>()
            });
            Instructores.Add(new Instructor()
            {
                Nombre = "Gaston",
                Apellido = "Argañaraz",
                Dni = "41333444",
                FechaNacimiento = DateTime.Now,
                Temas = new List<Tema>()
            });
            Instructores.Add(new Instructor()
            {
                Nombre = "Jhonny",
                Apellido = "Sang",
                Dni = "39888777",
                FechaNacimiento = DateTime.Now,
                Temas = new List<Tema>()
            });
        }
    }
}