USE [AdministradorDeProyectos]
GO

/****** Object:  Table [dbo].[Equipo]    Script Date: 15/9/2019 22:10:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Equipo](
	[IdEquipo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](75) NOT NULL,
	[Pais] [varchar](75) NOT NULL,
	[HusoHorario] [int] NOT NULL,
	[CantidadProgramadores] [int] NULL,
	[HorasDisponibleProgramadores] [float] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdEquipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


