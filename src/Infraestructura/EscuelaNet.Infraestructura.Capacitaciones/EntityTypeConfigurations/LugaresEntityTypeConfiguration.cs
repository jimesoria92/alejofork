﻿using System.Data.Entity.ModelConfiguration;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Infraestructura.Capacitaciones
{
    public class LugaresEntityTypeConfiguration : EntityTypeConfiguration<Lugar>
    {
        public LugaresEntityTypeConfiguration()
        {
            this.ToTable("Lugares");
            this.HasKey<int>(i => i.ID);
            this.Property(c => c.ID)
                .HasColumnName("IDLugar");
            this.Property(c => c.Calle)
                .IsRequired();
            this.Property(c => c.Capacidad)
                .IsRequired();
            this.Property(c => c.Localidad)
                .IsRequired();
            this.Property(c => c.Numero)
                .IsRequired();
            this.Property(c => c.Pais)
                .IsRequired();
            this.Property(c => c.Provincia)
                .IsRequired();
        }
    }
}