﻿using EscuelaNet.Dominio.Capacitaciones;
using System.Data.Entity.ModelConfiguration;

namespace EscuelaNet.Infraestructura.Capacitaciones
{
    public class TemasEntityTypeConfiguration : EntityTypeConfiguration<Tema>
    {
        public TemasEntityTypeConfiguration()
        {
            this.ToTable("Temas");
            this.HasKey<int>(i => i.ID);
            this.Property(c => c.ID)
                .HasColumnName("IDTema");
            this.Property(c => c.Nombre)
                .IsRequired();
            this.Property(c => c.Nivel)
                .HasColumnName("NivelTema");
        }
    }
}